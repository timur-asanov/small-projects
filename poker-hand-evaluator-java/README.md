# Poker Hand Evaluator

A simple evaluator of poker hands. Implemented in Java. Based on "Cactus Kev's" algorithm, located here: https://www.johnbelthoff.com/web-programming/poker-project/cactus-kev.aspx
