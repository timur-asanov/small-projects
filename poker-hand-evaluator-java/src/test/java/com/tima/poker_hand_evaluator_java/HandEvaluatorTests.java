package com.tima.poker_hand_evaluator_java;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.tima.poker_hand_evaluator_java.model.HandResult;
import com.tima.poker_hand_evaluator_java.model.HandResultName;

public class HandEvaluatorTests {

	@Test
	@DisplayName("evaluated one pairs of 2 card hands")
	public void testOnePairsOf2Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND2_RESULTS.get(HandResultName.ONE_PAIR);
			
			for (TestDataSet testDataSet : TestData.ONE_PAIRS_2CARD) {
				int result = HandEvaluator.eval2(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated high cards of 2 card hands")
	public void testHighCardsOf2Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND2_RESULTS.get(HandResultName.HIGH_CARD);
			
			for (TestDataSet testDataSet : TestData.HIGH_CARDS_2CARD) {
				int result = HandEvaluator.eval2(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated three of a kinds of 3 card hands")
	public void testThreeOfAKindsOf3Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND3_RESULTS.get(HandResultName.THREE_OF_A_KIND);
			
			for (TestDataSet testDataSet : TestData.THREE_OF_A_KINDS_3CARD) {
				int result = HandEvaluator.eval3(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), testDataSet.getCards().get(2).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated one pairs of 3 card hands")
	public void testOnePairsOf3Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND3_RESULTS.get(HandResultName.ONE_PAIR);
			
			for (TestDataSet testDataSet : TestData.ONE_PAIRS_3CARD) {
				int result = HandEvaluator.eval3(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), testDataSet.getCards().get(2).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated high cards of 3 card hands")
	public void testHighCardsOf3Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND3_RESULTS.get(HandResultName.HIGH_CARD);
			
			for (TestDataSet testDataSet : TestData.HIGH_CARDS_3CARD) {
				int result = HandEvaluator.eval3(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), testDataSet.getCards().get(2).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated straight flushes of 5 card hands")
	public void testStraightFlushesOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.STRAIGHT_FLUSH);
			
			for (TestDataSet testDataSet : TestData.STRAIGHT_FLUSHES_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated four of a kinds of 5 card hands")
	public void testFourOfAKindsOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.FOUR_OF_A_KIND);
			
			for (TestDataSet testDataSet : TestData.FOUR_OF_A_KINDS_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated full houses of 5 card hands")
	public void testFullHousesOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.FULL_HOUSE);
			
			for (TestDataSet testDataSet : TestData.FULL_HOUSES_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated flushes of 5 card hands")
	public void testFlushesOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.FLUSH);
			
			for (TestDataSet testDataSet : TestData.FLUSHES_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated straights of 5 card hands")
	public void testStraightsOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.STRAIGHT);
			
			for (TestDataSet testDataSet : TestData.STRAIGHTS_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated three of a kinds of 5 card hands")
	public void testThreeOfAKindsOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.THREE_OF_A_KIND);
			
			for (TestDataSet testDataSet : TestData.THREE_OF_A_KINDS_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated two pairs of 5 card hands")
	public void testTwoPairsOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.TWO_PAIR);
			
			for (TestDataSet testDataSet : TestData.TWO_PAIRS_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated one pairs of 5 card hands")
	public void testOnePairsOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.ONE_PAIR);
			
			for (TestDataSet testDataSet : TestData.ONE_PAIRS_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("evaluated high cards of 5 card hands")
	public void testHighCardsOf5Cards() {
		try {
			HandResult expectedHandResult = Constants.HAND5_RESULTS.get(HandResultName.HIGH_CARD);
			
			for (TestDataSet testDataSet : TestData.HIGH_CARDS_5CARD) {
				int result = HandEvaluator.eval5(testDataSet.getCards().get(0).getValue(), testDataSet.getCards().get(1).getValue(), 
						testDataSet.getCards().get(2).getValue(), testDataSet.getCards().get(3).getValue(), testDataSet.getCards().get(4).getValue());
				
				Assertions.assertTrue(result >= expectedHandResult.getMinValue(), "result should be greater than or equal to minimum expected value");
				Assertions.assertTrue(result <= expectedHandResult.getMaxValue(), "result should be less than or equal to maximum expected value");
				Assertions.assertEquals(testDataSet.getValue(), result, "result should be equal to expected value");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
