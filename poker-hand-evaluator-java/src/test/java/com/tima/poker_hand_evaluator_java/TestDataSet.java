package com.tima.poker_hand_evaluator_java;

import java.util.List;

import com.tima.poker_hand_evaluator_java.model.Card;

public class TestDataSet {

	private List<Card> cards = null;
	private int value = 0;
	
	public TestDataSet() {}

	public TestDataSet(List<Card> cards, int value) {
		this.cards = cards;
		this.value = value;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "TestDataSet [cards=" + cards + ", value=" + value + "]";
	}
}
