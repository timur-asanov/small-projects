package com.tima.poker_hand_evaluator_java;

import java.util.Arrays;
import java.util.List;

import com.tima.poker_hand_evaluator_java.model.Card;
import com.tima.poker_hand_evaluator_java.model.CardRank;
import com.tima.poker_hand_evaluator_java.model.CardSuit;

public class TestData {
	
	private static final Card ACE_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.ACE);
	private static final Card ACE_OF_SPADES = getCard(CardSuit.SPADES, CardRank.ACE);
	private static final Card ACE_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.ACE);
	private static final Card ACE_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.ACE);
	private static final Card KING_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.KING);
	private static final Card KING_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.KING);
	private static final Card KING_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.KING);
	private static final Card KING_OF_SPADES = getCard(CardSuit.SPADES, CardRank.KING);
	private static final Card QUEEN_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.QUEEN);
	private static final Card QUEEN_OF_SPADES = getCard(CardSuit.SPADES, CardRank.QUEEN);
	private static final Card QUEEN_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.QUEEN);
	private static final Card JACK_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.JACK);
	private static final Card JACK_OF_SPADES = getCard(CardSuit.SPADES, CardRank.JACK);
	private static final Card JACK_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.JACK);
	private static final Card JACK_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.JACK);
	private static final Card TEN_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.TEN);
	private static final Card TEN_OF_SPADES = getCard(CardSuit.SPADES, CardRank.TEN);
	private static final Card TEN_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.TEN);
	private static final Card TEN_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.TEN);
	private static final Card NINE_OF_SPADES = getCard(CardSuit.SPADES, CardRank.NINE);
	private static final Card NINE_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.NINE);
	private static final Card NINE_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.NINE);
	private static final Card EIGTH_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.EIGHT);
	private static final Card EIGTH_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.EIGHT);
	private static final Card SEVEN_OF_SPADES = getCard(CardSuit.SPADES, CardRank.SEVEN);
	private static final Card SEVEN_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.SEVEN);
	private static final Card SIX_OF_SPADES = getCard(CardSuit.SPADES, CardRank.SIX);
	private static final Card SIX_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.SIX);
	private static final Card SIX_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.SIX);
	private static final Card SIX_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.SIX);
	private static final Card FIVE_OF_SPADES = getCard(CardSuit.SPADES, CardRank.FIVE);
	private static final Card FIVE_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.FIVE);
	private static final Card FIVE_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.FIVE);
	private static final Card FIVE_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.FIVE);
	private static final Card FOUR_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.FOUR);
	private static final Card FOUR_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.FOUR);
	private static final Card FOUR_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.FOUR);
	private static final Card FOUR_OF_SPADES = getCard(CardSuit.SPADES, CardRank.FOUR);
	private static final Card THREE_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.THREE);
	private static final Card THREE_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.THREE);
	private static final Card THREE_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.THREE);
	private static final Card THREE_OF_SPADES = getCard(CardSuit.SPADES, CardRank.THREE);
	private static final Card TWO_OF_HEARTS = getCard(CardSuit.HEARTS, CardRank.TWO);
	private static final Card TWO_OF_CLUBS = getCard(CardSuit.CLUBS, CardRank.TWO);
	private static final Card TWO_OF_DIAMONDS = getCard(CardSuit.DIAMONDS, CardRank.TWO);
	private static final Card TWO_OF_SPADES = getCard(CardSuit.SPADES, CardRank.TWO);

	public static List<TestDataSet> ONE_PAIRS_2CARD;
	public static List<TestDataSet> HIGH_CARDS_2CARD;
	public static List<TestDataSet> THREE_OF_A_KINDS_3CARD;
	public static List<TestDataSet> ONE_PAIRS_3CARD;
	public static List<TestDataSet> HIGH_CARDS_3CARD;
	public static List<TestDataSet> STRAIGHT_FLUSHES_5CARD;
	public static List<TestDataSet> FOUR_OF_A_KINDS_5CARD;
	public static List<TestDataSet> FULL_HOUSES_5CARD;
	public static List<TestDataSet> FLUSHES_5CARD;
	public static List<TestDataSet> STRAIGHTS_5CARD;
	public static List<TestDataSet> THREE_OF_A_KINDS_5CARD;
	public static List<TestDataSet> TWO_PAIRS_5CARD;
	public static List<TestDataSet> ONE_PAIRS_5CARD;
	public static List<TestDataSet> HIGH_CARDS_5CARD;
	
	static {
		ONE_PAIRS_2CARD = getOnePairs2Card();
		HIGH_CARDS_2CARD = getHighCards2Card();
		THREE_OF_A_KINDS_3CARD = getThreeOfAKinds3Card();
		ONE_PAIRS_3CARD = getOnePairs3Card();
		HIGH_CARDS_3CARD = getHighCards3Card();
		STRAIGHT_FLUSHES_5CARD = getStraightFlushes5Card();
		FOUR_OF_A_KINDS_5CARD = getFourOfAKinds5Card();
		FULL_HOUSES_5CARD = getFullHouses5Card();
		FLUSHES_5CARD = getFlushes5Card();
		STRAIGHTS_5CARD = getStraights5Card();
		THREE_OF_A_KINDS_5CARD = getThreeOfAKinds5Card();
		TWO_PAIRS_5CARD = getTwoPairs5Card();
		ONE_PAIRS_5CARD = getOnePairs5Card();
		HIGH_CARDS_5CARD = getHighCards5Card();
	}

	private static List<TestDataSet> getOnePairs2Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, ACE_OF_SPADES), 0),
				new TestDataSet(Arrays.asList(EIGTH_OF_DIAMONDS, EIGTH_OF_HEARTS), 6),
				new TestDataSet(Arrays.asList(TWO_OF_HEARTS, TWO_OF_CLUBS), 12)
		);
	}
	
	private static List<TestDataSet> getHighCards2Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, KING_OF_CLUBS), 13),
				new TestDataSet(Arrays.asList(QUEEN_OF_DIAMONDS, FIVE_OF_HEARTS), 48),
				new TestDataSet(Arrays.asList(TWO_OF_CLUBS, THREE_OF_DIAMONDS), 90)
		);
	}

	private static List<TestDataSet> getThreeOfAKinds3Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, ACE_OF_SPADES, ACE_OF_CLUBS), 0),
				new TestDataSet(Arrays.asList(TWO_OF_DIAMONDS, TWO_OF_SPADES, TWO_OF_HEARTS), 12),
				new TestDataSet(Arrays.asList(TWO_OF_HEARTS, TWO_OF_CLUBS, TWO_OF_DIAMONDS), 12)
		);
	}

	private static List<TestDataSet> getOnePairs3Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, ACE_OF_SPADES, KING_OF_CLUBS), 13),
				new TestDataSet(Arrays.asList(EIGTH_OF_DIAMONDS, ACE_OF_SPADES, EIGTH_OF_HEARTS),48),
				new TestDataSet(Arrays.asList(TWO_OF_HEARTS, TWO_OF_CLUBS, THREE_OF_DIAMONDS), 168)
		);
	}
	
	private static List<TestDataSet> getHighCards3Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, QUEEN_OF_SPADES, KING_OF_CLUBS), 169),
				new TestDataSet(Arrays.asList(QUEEN_OF_DIAMONDS, SIX_OF_SPADES, FIVE_OF_HEARTS), 309),
				new TestDataSet(Arrays.asList(FOUR_OF_HEARTS, TWO_OF_CLUBS, THREE_OF_DIAMONDS), 454)
		);
	}
	
	private static List<TestDataSet> getStraightFlushes5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(TWO_OF_DIAMONDS, FOUR_OF_DIAMONDS, SIX_OF_DIAMONDS, THREE_OF_DIAMONDS, FIVE_OF_DIAMONDS), 9),
				new TestDataSet(Arrays.asList(TWO_OF_SPADES, THREE_OF_SPADES, FOUR_OF_SPADES, FIVE_OF_SPADES, ACE_OF_SPADES), 10),
				new TestDataSet(Arrays.asList(KING_OF_CLUBS, TEN_OF_CLUBS, JACK_OF_CLUBS, QUEEN_OF_CLUBS, NINE_OF_CLUBS), 2)
		);
	}
	
	private static List<TestDataSet> getFourOfAKinds5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, ACE_OF_SPADES, TEN_OF_HEARTS, ACE_OF_CLUBS, ACE_OF_HEARTS), 14),
				new TestDataSet(Arrays.asList(TWO_OF_DIAMONDS, TWO_OF_CLUBS, TWO_OF_SPADES, FOUR_OF_HEARTS, TWO_OF_HEARTS), 165),
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, ACE_OF_SPADES, JACK_OF_HEARTS, ACE_OF_CLUBS, ACE_OF_HEARTS), 13)
		);
	}
	
	private static List<TestDataSet> getFullHouses5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, ACE_OF_SPADES, FOUR_OF_HEARTS, FOUR_OF_CLUBS, ACE_OF_HEARTS), 176),
				new TestDataSet(Arrays.asList(TWO_OF_DIAMONDS, FOUR_OF_SPADES, TWO_OF_SPADES, FOUR_OF_HEARTS, TWO_OF_HEARTS), 321),
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, NINE_OF_SPADES, NINE_OF_HEARTS, NINE_OF_CLUBS, ACE_OF_HEARTS), 227)
		);
	}
	
	private static List<TestDataSet> getFlushes5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(KING_OF_DIAMONDS, TWO_OF_DIAMONDS, FIVE_OF_DIAMONDS, TEN_OF_DIAMONDS, QUEEN_OF_DIAMONDS), 875),
				new TestDataSet(Arrays.asList(TEN_OF_HEARTS, FIVE_OF_HEARTS, TWO_OF_HEARTS, FOUR_OF_HEARTS, SIX_OF_HEARTS), 1544),
				new TestDataSet(Arrays.asList(KING_OF_DIAMONDS, TWO_OF_DIAMONDS, FOUR_OF_DIAMONDS, TEN_OF_DIAMONDS, QUEEN_OF_DIAMONDS), 877)
		);
	}
	
	private static List<TestDataSet> getStraights5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(TWO_OF_DIAMONDS, THREE_OF_SPADES, FOUR_OF_HEARTS, FIVE_OF_DIAMONDS, SIX_OF_SPADES), 1608),
				new TestDataSet(Arrays.asList(ACE_OF_SPADES, TWO_OF_DIAMONDS, THREE_OF_CLUBS, FOUR_OF_HEARTS, FIVE_OF_HEARTS), 1609),
				new TestDataSet(Arrays.asList(KING_OF_SPADES, TEN_OF_SPADES, JACK_OF_HEARTS, QUEEN_OF_DIAMONDS, NINE_OF_CLUBS), 1601)
		);
	}
	
	private static List<TestDataSet> getThreeOfAKinds5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, ACE_OF_SPADES, TEN_OF_HEARTS, FOUR_OF_CLUBS, ACE_OF_HEARTS), 1645),
				new TestDataSet(Arrays.asList(TWO_OF_DIAMONDS, KING_OF_SPADES, TWO_OF_SPADES, FOUR_OF_HEARTS, TWO_OF_HEARTS), 2421),
				new TestDataSet(Arrays.asList(ACE_OF_DIAMONDS, ACE_OF_SPADES, NINE_OF_HEARTS, FOUR_OF_CLUBS, ACE_OF_HEARTS), 1652)
		);
	}
	
	private static List<TestDataSet> getTwoPairs5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(KING_OF_CLUBS, TWO_OF_DIAMONDS, KING_OF_HEARTS, JACK_OF_DIAMONDS, TWO_OF_CLUBS), 2712),
				new TestDataSet(Arrays.asList(KING_OF_SPADES, SEVEN_OF_CLUBS, KING_OF_HEARTS, FIVE_OF_HEARTS, FIVE_OF_SPADES), 2683),
				new TestDataSet(Arrays.asList(KING_OF_CLUBS, QUEEN_OF_DIAMONDS, KING_OF_SPADES, QUEEN_OF_CLUBS, JACK_OF_SPADES), 2601)
		);
	}
	
	private static List<TestDataSet> getOnePairs5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(NINE_OF_SPADES, FOUR_OF_HEARTS, FOUR_OF_CLUBS, THREE_OF_DIAMONDS, QUEEN_OF_DIAMONDS), 5645),
				new TestDataSet(Arrays.asList(FOUR_OF_SPADES, JACK_OF_SPADES, SIX_OF_CLUBS, TWO_OF_DIAMONDS, SIX_OF_DIAMONDS), 5248),
				new TestDataSet(Arrays.asList(ACE_OF_HEARTS, JACK_OF_HEARTS, FIVE_OF_DIAMONDS, ACE_OF_CLUBS, THREE_OF_CLUBS), 3457)
		);
	}
	
	private static List<TestDataSet> getHighCards5Card() {
		return Arrays.asList(
				new TestDataSet(Arrays.asList(KING_OF_HEARTS, NINE_OF_SPADES, FOUR_OF_CLUBS, THREE_OF_HEARTS, FIVE_OF_CLUBS), 6969),
				new TestDataSet(Arrays.asList(KING_OF_HEARTS, ACE_OF_SPADES, FIVE_OF_CLUBS, NINE_OF_CLUBS, SEVEN_OF_SPADES), 6301),
				new TestDataSet(Arrays.asList(SIX_OF_DIAMONDS, QUEEN_OF_SPADES, JACK_OF_HEARTS, TEN_OF_CLUBS, FIVE_OF_DIAMONDS), 7025)
		);
	}
	
	private static Card getCard(CardSuit cardSuit, CardRank cardRank) {
		for(Card c : Constants.GENERATED_CARDS) {
			if (c.getSuit() == cardSuit && c.getRank() == cardRank) {
				return c;
			}
		}
		
		return null;
	}
}
