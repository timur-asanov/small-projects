package com.tima.poker_hand_evaluator_java.model;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DeckTests {

	private Deck deck = null;
	
	@BeforeEach
	public void beforeEach() {
		try {
			this.deck = new Deck();
			
			Assertions.assertNotNull(this.deck, "deck should not be null");
			Assertions.assertNotNull(this.deck.getCards(), "deck cards should not be null");
			Assertions.assertEquals(this.deck.getCards().size(), 52, "deck should have 52 cards");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("got 7 random cards")
	public void testGetRandom7() {
		try {
			List<Card> cards = this.deck.getRandom7();
			
			Assertions.assertNotNull(cards, "cards should not be null");
			Assertions.assertEquals(cards.size(), 7, "there should be 7 cards");
			Assertions.assertEquals(this.deck.getCards().size(), 45, "there should be 45 cards left");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("got 6 random cards")
	public void testGetRandom6() {
		try {
			List<Card> cards = this.deck.getRandom6();
			
			Assertions.assertNotNull(cards, "cards should not be null");
			Assertions.assertEquals(cards.size(), 6, "there should be 6 cards");
			Assertions.assertEquals(this.deck.getCards().size(), 46, "there should be 46 cards left");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("got 5 random cards")
	public void testGetRandom5() {
		try {
			List<Card> cards = this.deck.getRandom5();
			
			Assertions.assertNotNull(cards, "cards should not be null");
			Assertions.assertEquals(cards.size(), 5, "there should be 5 cards");
			Assertions.assertEquals(this.deck.getCards().size(), 47, "there should be 47 cards left");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("got 3 random cards")
	public void testGetRandom3() {
		try {
			List<Card> cards = this.deck.getRandom3();
			
			Assertions.assertNotNull(cards, "cards should not be null");
			Assertions.assertEquals(cards.size(), 3, "there should be 3 cards");
			Assertions.assertEquals(this.deck.getCards().size(), 49, "there should be 49 cards left");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("got 2 random cards")
	public void testGetRandom2() {
		try {
			List<Card> cards = this.deck.getRandom2();
			
			Assertions.assertNotNull(cards, "cards should not be null");
			Assertions.assertEquals(cards.size(), 2, "there should be 2 cards");
			Assertions.assertEquals(this.deck.getCards().size(), 50, "there should be 50 cards left");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("got 1 random card")
	public void testGetRandomCard() {
		try {
			List<Card> cards = this.deck.getRandomCard();
			
			Assertions.assertNotNull(cards, "cards should not be null");
			Assertions.assertEquals(cards.size(), 1, "there should be 1 card");
			Assertions.assertEquals(this.deck.getCards().size(), 51, "there should be 51 cards left");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("got random cards multiple times")
	public void testGetRandomCards() {
		try {
			List<Card> firstFive = this.deck.getRandom5();
			
			Assertions.assertNotNull(firstFive, "first set of cards should not be null");
			Assertions.assertEquals(firstFive.size(), 5, "there should be 5 cards as first set");
			Assertions.assertEquals(this.deck.getCards().size(), 47, "there should be 47 cards left");
			
			List<Card> secondThree = this.deck.getRandom3();
			
			Assertions.assertNotNull(secondThree, "second set of cards should not be null");
			Assertions.assertEquals(secondThree.size(), 3, "there should be 3 cards as second set");
			Assertions.assertEquals(this.deck.getCards().size(), 44, "there should be 44 cards left");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	left here as a way to test generating a deck
//	@Test
//	@DisplayName("generated deck")
//	public void testGenerateDeck() {
//		try {
//			List<Card> generatedDeck = Deck.generateDeck();
//			
//			Assertions.assertNotNull(generatedDeck, "generated deck should not be null");
//			Assertions.assertEquals(generatedDeck.size(), 52, "there should be 52 cards");
//			Assertions.assertEquals(generatedDeck, this.deck.getCards(), "the generated deck should be equal to a pre-generated deck");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
