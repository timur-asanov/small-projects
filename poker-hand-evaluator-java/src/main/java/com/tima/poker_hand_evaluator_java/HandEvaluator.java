package com.tima.poker_hand_evaluator_java;

public class HandEvaluator {

	public static int eval5(int card1Value, int card2Value, int card3Value, int card4Value, int card5Value) {
		int temp1 = (card1Value | card2Value | card3Value | card4Value | card5Value) >> 16;

        if ((card1Value & card2Value & card3Value & card4Value & card5Value & 0xf000) != 0) {
            return Constants.FLUSHES[temp1];
        }

        int temp2 = Constants.UNIQUE_5[temp1];

        if (temp2 != 0) {
            return temp2;
        }

        return Constants.HASH_VALUES[evalTemp((card1Value & 0xff) * (card2Value & 0xff) * (card3Value & 0xff) * (card4Value & 0xff) * (card5Value & 0xff))];
	}
	
	public static int eval3(int card1Value, int card2Value, int card3Value) {
		return Constants.HAND_3S.indexOf((card1Value & 0xff) * (card2Value & 0xff) * (card3Value & 0xff));
	}
	
	public static int eval2(int card1Value, int card2Value) {
		return Constants.HAND_2S.indexOf((card1Value & 0xff) * (card2Value & 0xff));
	}
	
	private static int evalTemp(int input) {
		input += 0xe91aaa35;
		input ^= input >>> 16;
		input += input << 8;
		input ^= input >>> 4;

        int temp1 = (input >>> 8) & 0x1ff;
        int temp2 = (input + (input << 2)) >>> 19;
        int result = temp2 ^ Constants.HASH_ADJUST[temp1];
        
        return result;
	}
}
