package com.tima.poker_hand_evaluator_java.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.tima.poker_hand_evaluator_java.Constants;

public class Deck {

	private List<Card> cards = null;
	
	public Deck() {
		this.cards = new ArrayList<Card>(Arrays.asList(Constants.GENERATED_CARDS));
	}
	
	public List<Card> getRandom7() {
		return this.getRandomCards(7);
	}
	
	public List<Card> getRandom6() {
		return this.getRandomCards(6);
	}
	
	public List<Card> getRandom5() {
		return this.getRandomCards(5);
	}
	
	public List<Card> getRandom3() {
		return this.getRandomCards(3);
	}
	
	public List<Card> getRandom2() {
		return this.getRandomCards(2);
	}
	
	public List<Card> getRandomCard() {
		return this.getRandomCards(1);
	}
	
	private List<Card> getRandomCards(int numberToGet) {
		List<Card> result = null;
		
		if (this.cards != null && this.cards.size() >= numberToGet) {
			result = new ArrayList<Card>();
			
			for (int i = 0; i < numberToGet; i++) {
				int index = new Random().nextInt(this.cards.size());
				result.add(this.cards.get(index));
				this.cards.remove(index);
			}
		}
		
		return result;
	}
	
	public List<Card> getCards() {
		return this.cards;
	}
	
//	left here as a way to generate cards and their values if needed
//	public static List<Card> generateDeck() {
//		List<Card> deck = new ArrayList<Card>();
//		
//		for (CardSuit cardSuit : CardSuit.values()) {
//			for (int i = 0; i < CardRank.values().length; i++) {
//				int cardValue = generateCardValue(i, cardSuit.getValue());
//				
//				deck.add(new Card(cardSuit, CardRank.values()[i], cardValue));
//			}
//		}
//		
//		return deck;
//	}
//	
//	private static int generateCardValue(int cardRankIndex, int cardSuitValue) {
//		int cardValue = Constants.PRIMES[cardRankIndex] | (cardRankIndex << 8) | cardSuitValue | (1 << (16 + cardRankIndex));
//		
//		return cardValue;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + ((cards == null) ? 0 : cards.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Deck other = (Deck) obj;
		
		if (cards == null) {
			if (other.cards != null)
				return false;
		} else if (!cards.equals(other.cards))
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder("Deck [\n");
		
		if (this.cards != null && this.cards.size() > 0) {
			for (Card card : this.cards) {
				stringBuilder.append("\t" + card.toString() + "\n");
			}
		} else {
			stringBuilder.append("empty\n");
		}		
		
		stringBuilder.append("]");
		
		return stringBuilder.toString();
	}
}
