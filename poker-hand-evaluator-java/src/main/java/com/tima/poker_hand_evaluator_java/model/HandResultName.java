package com.tima.poker_hand_evaluator_java.model;

public enum HandResultName {

	STRAIGHT_FLUSH("Straight Flush"),
	FOUR_OF_A_KIND("Four of a Kind"),
	FULL_HOUSE("Full House"),
	FLUSH("Flush"),
	STRAIGHT("Straight"),
	THREE_OF_A_KIND("Three of a Kind"),
	TWO_PAIR("Two Pair"),
	ONE_PAIR("One Pair"),
	HIGH_CARD("High Card");
	
	private String text = null;
	
	private HandResultName(String text) {
		this.text = text;
	}
	
	public String getText() {
		return this.text;
	}
}
