package com.tima.poker_hand_evaluator_java.model;

public class HandResult {

	private HandResultName name = null;
	private int minValue = 0;
	private int maxValue = 0;
	
	public HandResult() {}

	public HandResult(HandResultName name, int minValue, int maxValue) {
		this.name = name;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public HandResultName getName() {
		return name;
	}

	public void setName(HandResultName name) {
		this.name = name;
	}

	public int getMinValue() {
		return minValue;
	}

	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}

	public int getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	@Override
	public String toString() {
		return "HandResult [name=" + name + ", minValue=" + minValue + ", maxValue=" + maxValue + "]";
	}
}
