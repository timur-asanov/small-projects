package com.tima.poker_hand_evaluator_java.model;

public class Card {

	private CardSuit suit;
	private CardRank rank;
	private int value;
	
	public Card() {}

	public Card(CardSuit suit, CardRank rank, int value) {
		this.suit = suit;
		this.rank = rank;
		this.value = value;
	}

	public CardSuit getSuit() {
		return suit;
	}

	public void setSuit(CardSuit suit) {
		this.suit = suit;
	}

	public CardRank getRank() {
		return rank;
	}

	public void setRank(CardRank rank) {
		this.rank = rank;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + ((rank == null) ? 0 : rank.hashCode());
		result = prime * result + ((suit == null) ? 0 : suit.hashCode());
		result = prime * result + value;
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Card other = (Card) obj;
		
		if (rank != other.rank)
			return false;
		
		if (suit != other.suit)
			return false;
		
		if (value != other.value)
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		return "Card [suit=" + suit + ", rank=" + rank + ", value=" + value + "]";
	}
}
