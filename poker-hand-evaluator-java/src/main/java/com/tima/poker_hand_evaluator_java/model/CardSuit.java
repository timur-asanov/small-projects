package com.tima.poker_hand_evaluator_java.model;

public enum CardSuit {
	
	CLUBS(0x8000), 
	SPADES(0x1000), 
	HEARTS(0x2000), 
	DIAMONDS(0x4000);
	
	private int value = 0;

	private CardSuit(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
}
