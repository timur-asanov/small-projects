# Small Projects

[![pipeline status](https://gitlab.com/timur-asanov/small-projects/badges/master/pipeline.svg)](https://gitlab.com/timur-asanov/small-projects/-/commits/master)

This repository contains mini projects / implementations.
* Poker Hand Evaluator (JS, Electron, React)
* Poker Hand Evaluator (JAVA)

More info: https://timur-asanov.gitlab.io/small-projects

