# Poker Hand Evaluator

A simple evaluator of poker hands. Built in Electron to try it out. Based on "Cactus Kev's" algorithm, located here: https://www.johnbelthoff.com/web-programming/poker-project/cactus-kev.aspx

### Usage
1. Clone the respository or download it as a zip
2. Run `npm install`
3. Run `npm run package`
4. Based on your OS, electron will package the code into an application usable on your system, and put it into a new folder at the root level of the project.

### What is included
##### There is a poker hand evaluator in which you can play a hand of poker.
<img src="static/poker_hand.gif" width="60%" height="60%" alt="Poker Hand GIF" />
##### There are 5 hand evaluators for 2 card, 3 card, 5 card, 6 card and 7 card hands.
<img src="static/hand_evaluators.gif" width="60%" height="60%" alt="Hand Evaluators GIF" />