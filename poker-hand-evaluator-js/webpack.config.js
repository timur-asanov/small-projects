const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    watch: true,
    target: 'electron-renderer',
    entry: './app/src/index.js',
    output: {
        pathinfo: true,
        filename: 'js/bundle.js',
        chunkFilename: 'js/[name].[hash].chunk.js',
        publicPath: ''
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                options: {
                    presets: ['react']
                }
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    loader: 'css-loader',
                    options: {
                        modules: true,
                        localIdentName: '[local]__[hash:base64:5]'
                    }
                })
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                query: {
                    name: '[name].[hash].[ext]'
                }
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'css/bundle.css',
            disable: false,
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            inject: true,
            template: __dirname + '/app/public/index.html',
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),
    ],
    resolve: {
        extensions: ['.js', '.json', '.jsx']
    }
};
