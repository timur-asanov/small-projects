/* eslint-disable no-console */

const { app, BrowserWindow } = require('electron');

let win;

const isProd = process.execPath.search('electron') === -1;

if (isProd === false) {
    require('electron-reload')(__dirname);
}

const installExtensions = async () => {
    const installer = require('electron-devtools-installer');
    const extensions = [
        'REACT_DEVELOPER_TOOLS'
    ];

    return Promise
            .all(extensions.map(name => installer.default(installer[name], true)))
            .catch(console.log);
};

function createWindow () {
    win = new BrowserWindow({width: 800, height: 600});
    win.loadURL(`file://${__dirname}/dist/index.html`);

    if (isProd === false) {
        win.webContents.openDevTools();
    }

    win.on('closed', () => {
      win = null;
  });
}

if (isProd === true) {
    app.on('ready', createWindow);
} else {
    app.on('ready', async () => {
        await installExtensions();
        createWindow();
    });
}

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});
