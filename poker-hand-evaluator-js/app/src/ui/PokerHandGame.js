import React from 'react';

import PokerGame from '../game_logic/PokerGame';

class PokerHandGame extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            pokerButtonLabel: 'Play'
        };

        this.playPoker = this.playPoker.bind(this);
    }

    playPoker() {
        let pokerHand;
        let pokerRound = this.state.pokerRound;
        let pokerButtonLabel;

        if (pokerRound === undefined || pokerRound === 4) {
            this.pokerGame = new PokerGame();

            pokerHand = this.pokerGame.startGame();
            pokerRound = 1;
            pokerButtonLabel = 'Flop';
        } else {
            switch(this.state.pokerRound) {
                case 1:
                    pokerHand = this.pokerGame.flop();
                    pokerButtonLabel = 'Turn';
                    break;
                case 2:
                    pokerHand = this.pokerGame.turn();
                    pokerButtonLabel = 'River';
                    break;
                case 3:
                    pokerHand = this.pokerGame.river();
                    pokerButtonLabel = 'Play Again';
                    break;
            }

            pokerRound++;
        }

        this.setState({
            pokerHand: pokerHand,
            pokerRound: pokerRound,
            pokerButtonLabel: pokerButtonLabel
        });
    }

    render() {
        return (
            <div>
                {this.state.pokerHand && this.state.pokerRound >= 1 &&
                    <div>
                        <h3>Your hand:</h3>
                        <ul>
                            <li>{this.state.pokerHand.cards[0].toString()}</li>
                            <li>{this.state.pokerHand.cards[1].toString()}</li>
                        </ul>
                    </div>
                }

                {this.state.pokerHand && this.state.pokerRound > 1 && this.state.pokerRound <= 4 &&
                    <div>
                        <h3>Table:</h3>
                        <ul>
                            {this.state.pokerHand.cards[2] &&
                                <li>{this.state.pokerHand.cards[2].toString()}</li>
                            }
                            {this.state.pokerHand.cards[3] &&
                                <li>{this.state.pokerHand.cards[3].toString()}</li>
                            }
                            {this.state.pokerHand.cards[4] &&
                                <li>{this.state.pokerHand.cards[4].toString()}</li>
                            }
                            {this.state.pokerHand.cards[5] &&
                                <li>{this.state.pokerHand.cards[5].toString()}</li>
                            }
                            {this.state.pokerHand.cards[6] &&
                                <li>{this.state.pokerHand.cards[6].toString()}</li>
                            }
                        </ul>
                    </div>
                }

                {this.state.pokerHand && this.state.pokerHand.result &&
                    <h3>Result: {this.state.pokerHand.result.name}</h3>
                }

                <p>
                    <button onClick={this.playPoker}>{this.state.pokerButtonLabel}</button>
                </p>
            </div>
        );
    }
}

export default PokerHandGame;
