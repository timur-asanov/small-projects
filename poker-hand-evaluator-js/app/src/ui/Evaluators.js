import React from 'react';

import Game from '../game_logic/Game';

class Evaluators extends React.Component {

    constructor(props) {
        super(props);

        this.game = new Game();

        const time7start = performance.now();
        const hand7 = this.game.play7();
        const time7end = performance.now();
        const game7time = timeElapsed(time7end - time7start);

        const time6start = performance.now();
        const hand6 = this.game.play6();
        const time6end = performance.now();
        const game6time = timeElapsed(time6end - time6start);

        const time5start = performance.now();
        const hand5 = this.game.play5();
        const time5end = performance.now();
        const game5time = timeElapsed(time5end - time5start);

        const time3start = performance.now();
        const hand3 = this.game.play3();
        const time3end = performance.now();
        const game3time = timeElapsed(time3end - time3start);

        const time2start = performance.now();
        const hand2 = this.game.play2();
        const time2end = performance.now();
        const game2time = timeElapsed(time2end - time2start);

        this.state = {
            hand7: hand7,
            game7time: game7time,
            hand6: hand6,
            game6time: game6time,
            hand5: hand5,
            game5time: game5time,
            hand3: hand3,
            game3time: game3time,
            hand2: hand2,
            game2time: game2time
        };

        this.play7round = this.play7round.bind(this);
        this.play6round = this.play6round.bind(this);
        this.play5round = this.play5round.bind(this);
        this.play3round = this.play3round.bind(this);
        this.play2round = this.play2round.bind(this);
    }

    play7round() {
        const timestart = performance.now();
        const hand = this.game.play7();
        const timeend = performance.now();
        const gametime = timeElapsed(timeend - timestart);

        this.setState({
            hand7: hand,
            game7time: gametime
        });
    }

    play6round() {
        const timestart = performance.now();
        const hand = this.game.play6();
        const timeend = performance.now();
        const gametime = timeElapsed(timeend - timestart);

        this.setState({
            hand6: hand,
            game6time: gametime
        });
    }

    play5round() {
        const timestart = performance.now();
        const hand = this.game.play5();
        const timeend = performance.now();
        const gametime = timeElapsed(timeend - timestart);

        this.setState({
            hand5: hand,
            game5time: gametime
        });
    }

    play3round() {
        const timestart = performance.now();
        const hand = this.game.play3();
        const timeend = performance.now();
        const gametime = timeElapsed(timeend - timestart);

        this.setState({
            hand3: hand,
            game3time: gametime
        });
    }

    play2round() {
        const timestart = performance.now();
        const hand = this.game.play2();
        const timeend = performance.now();
        const gametime = timeElapsed(timeend - timestart);

        this.setState({
            hand2: hand,
            game2time: gametime
        });
    }

    render() {
        return (
            <div>
                <div style={{"width": "100%", "clear": "both", "float": "left"}}>
                    <div style={{"width": "33%", "float": "left"}}>
                        <h3>Your hand:</h3>
                        <ul>
                            {this.state.hand5.cards.map((card) => (
                                <li key={card}>{card.toString()}</li>
                            ))}
                        </ul>

                        <h3>Result:</h3>
                        <p>
                            {this.state.hand5.result.name}{' ('}{this.state.game5time}{' ms)'}
                        </p>

                        <p>
                            <button onClick={this.play5round}>Play 5 Cards Again</button>
                        </p>
                    </div>
                    <div style={{"width": "33%", "float": "left"}}>
                        <h3>Your hand:</h3>
                        <ul>
                            {this.state.hand3.cards.map((card) => (
                                <li key={card}>{card.toString()}</li>
                            ))}
                        </ul>

                        <h3>Result:</h3>
                        <p>
                            {this.state.hand3.result.name}{' ('}{this.state.game3time}{' ms)'}
                        </p>

                        <p>
                            <button onClick={this.play3round}>Play 3 Cards Again</button>
                        </p>
                    </div>
                    <div style={{"width": "33%", "float": "left"}}>
                        <h3>Your hand:</h3>
                        <ul>
                            {this.state.hand2.cards.map((card) => (
                                <li key={card}>{card.toString()}</li>
                            ))}
                        </ul>

                        <h3>Result:</h3>
                        <p>
                            {this.state.hand2.result.name}{' ('}{this.state.game2time}{' ms)'}
                        </p>

                        <p>
                            <button onClick={this.play2round}>Play 2 Cards Again</button>
                        </p>
                    </div>
                </div>
                <div style={{"width": "100%", "clear": "both", "float": "left"}}>
                    <div style={{"width": "33%", "float": "left"}}>
                        <h3>Your hand:</h3>
                        <ul>
                            {this.state.hand6.cards.map((card) => (
                                <li key={card}>{card.toString()}</li>
                            ))}
                        </ul>

                        <h3>Result:</h3>
                        <p>
                            {this.state.hand6.result.name}{' ('}{this.state.game6time}{' ms)'}
                        </p>

                        <p>
                            <button onClick={this.play6round}>Play 6 Cards Again</button>
                        </p>
                    </div>
                    <div style={{"width": "33%", "float": "left"}}>
                        <h3>Your hand:</h3>
                        <ul>
                            {this.state.hand7.cards.map((card) => (
                                <li key={card}>{card.toString()}</li>
                            ))}
                        </ul>

                        <h3>Result:</h3>
                        <p>
                            {this.state.hand7.result.name}{' ('}{this.state.game7time}{' ms)'}
                        </p>

                        <p>
                            <button onClick={this.play7round}>Play 7 Cards Again</button>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Evaluators;

function timeElapsed(time) {
    return +(Math.round(time + "e+2")  + "e-2");
}
