import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import PokerHandGame from './ui/PokerHandGame';
import Evaluators from './ui/Evaluators';

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <ul>
                        <li><Link to="/pokerhand">Play a Poker Hand</Link></li>
                        <li><Link to="/evaluators">Hand Evaluators</Link></li>
                    </ul>

                    <Route path="/pokerhand" component={PokerHandGame} />
                    <Route path="/evaluators" component={Evaluators} />
                </div>
            </Router>
        );
    }
}

export default App;
