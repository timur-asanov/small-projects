import { hashAdjust, hashValues, flushes, unique5, hand3s, hand2s } from './generated';

class HandEvaluator {
    constructor() {}

    eval5(card1, card2, card3, card4, card5) {
        const temp1 = (card1 | card2 | card3 | card4 | card5) >> 16;

        if ((card1 & card2 & card3 & card4 & card5 & 0xf000) != 0) {
            return flushes[temp1];
        }

        const temp2 = unique5[temp1];

        if (temp2 != 0) {
            return temp2;
        }

        return hashValues[this.evalTemp((card1 & 0xff) * (card2 & 0xff) * (card3 & 0xff) * (card4 & 0xff) * (card5 & 0xff))];
    }

    eval3(card1, card2, card3) {
        return hand3s.indexOf((card1 & 0xff) * (card2 & 0xff) * (card3 & 0xff));
    }

    eval2(card1, card2) {
        return hand2s.indexOf((card1 & 0xff) * (card2 & 0xff));
    }

    evalTemp(item) {
        item += 0xe91aaa35;
        item ^= item >>> 16;
        item += item << 8;
        item ^= item >>> 4;

        const temp1 = (item >>> 8) & 0x1ff;
        const temp2 = (item + (item << 2)) >>> 19;
        const result = temp2 ^ hashAdjust[temp1];

        return result;
    }

    /* left here as a way to generate cards and their values if needed */
    // static generateCardValue(rankIndex, suitValue) {
    //     const cardValue = primes[rankIndex] | (rankIndex << 8) | suitValue | (1 << (16 + rankIndex));

    //     return cardValue;
    // }
}

export default HandEvaluator;
