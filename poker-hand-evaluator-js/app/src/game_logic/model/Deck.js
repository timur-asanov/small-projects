import Card from './Card';
import { generatedCards } from '../generated';

class Deck {
    constructor() {
        this.cards = [];
        this.generateDeck();
    }

    generateDeck() {
        generatedCards.map((generatedCard) => {
            const card = new Card(generatedCard.rank, generatedCard.suit, generatedCard.value);

            this.cards.push(card);
        });
    }

    getRandom7() {
        return this.getRandomCards(7);
    }

    getRandom6() {
        return this.getRandomCards(6);
    }

    getRandom5() {
        return this.getRandomCards(5);
    }

    getRandom3() {
        return this.getRandomCards(3);
    }

    getRandom2() {
        return this.getRandomCards(2);
    }

    getRandomCard() {
        return this.getRandomCards(1);
    }

    getRandomCards(numberToGet) {
        const hand = [];

        if (this.cards.length >= numberToGet) {
            for (let i = 0; i < numberToGet; i++) {
                hand.push(this.cards.splice(Math.floor(Math.random() * this.cards.length), 1)[0]);
            }
        }

        return hand;
    }

    /* left here as a way to generate cards and their values if needed */
    // generateDeck() {
    //     Object.entries(CardSuit).forEach(
    //         ([suit, value]) => {
    //             Object.entries(CardRank).forEach(
    //                 ([rank], index) => {
    //                     const cardValue = HandEvaluator.generateCardValue(index, value);

    //                     const card = new Card(rank, suit, cardValue);

    //                     this.cards.push(card);
    //                 }
    //             );
    //         }
    //     );
    // }
}

export default Deck;
