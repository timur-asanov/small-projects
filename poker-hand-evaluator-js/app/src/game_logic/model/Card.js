class Card {
    constructor(rank, suit, value) {
        this.rank = rank;
        this.suit = suit;
        this.value = value;
    }

    toString() {
        return this.rank + ' of ' + this.suit;
    }
}

export default Card;
