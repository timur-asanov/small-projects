import Deck from './Deck';

test('deck contains 52 cards', () => {
    const deck = new Deck();

    expect(deck.cards).toHaveLength(52);
});

test('got 7 cards for hand', () => {
    const deck = new Deck();
    const hand = deck.getRandom7();

    expect(hand).toHaveLength(7);
});

test('got 6 cards for hand', () => {
    const deck = new Deck();
    const hand = deck.getRandom6();

    expect(hand).toHaveLength(6);
});

test('got 5 cards for hand', () => {
    const deck = new Deck();
    const hand = deck.getRandom5();

    expect(hand).toHaveLength(5);
});

test('got 3 cards for hand', () => {
    const deck = new Deck();
    const hand = deck.getRandom3();

    expect(hand).toHaveLength(3);
});

test('got 2 cards for hand', () => {
    const deck = new Deck();
    const hand = deck.getRandom2();

    expect(hand).toHaveLength(2);
});
