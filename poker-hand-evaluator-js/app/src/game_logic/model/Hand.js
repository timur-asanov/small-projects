class Hand {
    constructor(cards = [], value = 0, result) {
        this.cards = cards;
        this.value = value;
        this.result = result;
    }
}

export default Hand;
