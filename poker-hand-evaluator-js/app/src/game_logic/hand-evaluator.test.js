import HandEvaluator from './HandEvaluator';
import { hand5Results, hand3Results, hand2Results } from './constants';

import {
    highCards, onePairs, twoPairs,
    threeOfAKinds, straights, flushes,
    fullHouses, fourOfAKinds, straightFlushes
} from './test/test-data-5-card';

import { highCards_3card, onePairs_3card, threeOfAKinds_3card } from './test/test-data-3-card';
import { highCards_2card, onePairs_2card } from './test/test-data-2-card';

test('evaluated 5 card high cards', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[8];

    highCards.map((highCard) => {
        const evalValue = handEvaluator.eval5(
                                              highCard.cards[0].value,
                                              highCard.cards[1].value,
                                              highCard.cards[2].value,
                                              highCard.cards[3].value,
                                              highCard.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(highCard.value);
    });
});

test('evaluated 5 card one pairs', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[7];

    onePairs.map((onePair) => {
        const evalValue = handEvaluator.eval5(
                                              onePair.cards[0].value,
                                              onePair.cards[1].value,
                                              onePair.cards[2].value,
                                              onePair.cards[3].value,
                                              onePair.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(onePair.value);
    });
});

test('evaluated 5 card two pairs', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[6];

    twoPairs.map((twoPair) => {
        const evalValue = handEvaluator.eval5(
                                              twoPair.cards[0].value,
                                              twoPair.cards[1].value,
                                              twoPair.cards[2].value,
                                              twoPair.cards[3].value,
                                              twoPair.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(twoPair.value);
    });
});

test('evaluated 5 card three of a kinds', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[5];

    threeOfAKinds.map((threeOfAKind) => {
        const evalValue = handEvaluator.eval5(
                                              threeOfAKind.cards[0].value,
                                              threeOfAKind.cards[1].value,
                                              threeOfAKind.cards[2].value,
                                              threeOfAKind.cards[3].value,
                                              threeOfAKind.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(threeOfAKind.value);
    });
});

test('evaluated 5 card straights', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[4];

    straights.map((straight) => {
        const evalValue = handEvaluator.eval5(
                                              straight.cards[0].value,
                                              straight.cards[1].value,
                                              straight.cards[2].value,
                                              straight.cards[3].value,
                                              straight.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(straight.value);
    });
});

test('evaluated 5 card flushes', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[3];

    flushes.map((flush) => {
        const evalValue = handEvaluator.eval5(
                                              flush.cards[0].value,
                                              flush.cards[1].value,
                                              flush.cards[2].value,
                                              flush.cards[3].value,
                                              flush.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(flush.value);
    });
});

test('evaluated 5 card full houses', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[2];

    fullHouses.map((fullHouse) => {
        const evalValue = handEvaluator.eval5(
                                              fullHouse.cards[0].value,
                                              fullHouse.cards[1].value,
                                              fullHouse.cards[2].value,
                                              fullHouse.cards[3].value,
                                              fullHouse.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(fullHouse.value);
    });
});

test('evaluated 5 card four of a kinds', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[1];

    fourOfAKinds.map((fourOfAKind) => {
        const evalValue = handEvaluator.eval5(
                                              fourOfAKind.cards[0].value,
                                              fourOfAKind.cards[1].value,
                                              fourOfAKind.cards[2].value,
                                              fourOfAKind.cards[3].value,
                                              fourOfAKind.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(fourOfAKind.value);
    });
});

test('evaluated 5 card straight flushes', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand5Results[0];

    straightFlushes.map((straightFlush) => {
        const evalValue = handEvaluator.eval5(
                                              straightFlush.cards[0].value,
                                              straightFlush.cards[1].value,
                                              straightFlush.cards[2].value,
                                              straightFlush.cards[3].value,
                                              straightFlush.cards[4].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(straightFlush.value);
    });
});

test('evaluated 3 card high cards', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand3Results[2];

    highCards_3card.map((highCard) => {
        const evalValue = handEvaluator.eval3(
                                              highCard.cards[0].value,
                                              highCard.cards[1].value,
                                              highCard.cards[2].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(highCard.value);
    });
});

test('evaluated 3 card one pairs', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand3Results[1];

    onePairs_3card.map((onePair) => {
        const evalValue = handEvaluator.eval3(
                                              onePair.cards[0].value,
                                              onePair.cards[1].value,
                                              onePair.cards[2].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(onePair.value);
    });
});

test('evaluated 3 card three of a kinds', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand3Results[0];

    threeOfAKinds_3card.map((threeOfAKind) => {
        const evalValue = handEvaluator.eval3(
                                              threeOfAKind.cards[0].value,
                                              threeOfAKind.cards[1].value,
                                              threeOfAKind.cards[2].value
                                            );

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(threeOfAKind.value);
    });
});

test('evaluated 2 card high cards', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand2Results[1];

    highCards_2card.map((highCard) => {
        const evalValue = handEvaluator.eval2(highCard.cards[0].value, highCard.cards[1].value);

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(highCard.value);
    });
});

test('evaluated 2 card one pairs', () => {
    const handEvaluator = new HandEvaluator();
    const handResult = hand2Results[0];

    onePairs_2card.map((onePair) => {
        const evalValue = handEvaluator.eval2(onePair.cards[0].value, onePair.cards[1].value);

        expect(evalValue).toBeDefined();
        expect(evalValue).toBeGreaterThanOrEqual(handResult.minValue);
        expect(evalValue).toBeLessThanOrEqual(handResult.maxValue);
        expect(evalValue).toBe(onePair.value);
    });
});
