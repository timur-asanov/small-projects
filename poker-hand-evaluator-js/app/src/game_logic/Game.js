import Deck from './model/Deck';
import Hand from './model/Hand';
import { hand5Results, hand3Results, hand2Results, card6combinations, card7combinations } from './constants';
import HandEvaluator from './HandEvaluator';
import GameUtils from './utils';

class Game {
    constructor() {
        this.handEvaluator = new HandEvaluator();
    }

    play7() {
        return this.generateHand(7);
    }

    play6() {
        return this.generateHand(6);
    }

    play5() {
        return this.generateHand(5);
    }

    play3() {
        return this.generateHand(3);
    }

    play2() {
        return this.generateHand(2);
    }

    generateHand(numCards) {
        const deck = new Deck();
        const hand = new Hand();

        switch (numCards) {
            case 2: {
                hand.cards = deck.getRandom2();
                hand.value = this.handEvaluator.eval2(hand.cards[0].value, hand.cards[1].value);
                hand.result = GameUtils.findHandResult(hand.value, hand2Results);
                break;
            }
            case 3: {
                hand.cards = deck.getRandom3();
                hand.value = this.handEvaluator.eval3(hand.cards[0].value, hand.cards[1].value, hand.cards[2].value);
                hand.result = GameUtils.findHandResult(hand.value, hand3Results);
                break;
            }
            case 5: {
                hand.cards = deck.getRandom5();
                hand.value = this.handEvaluator.eval5(
                                                        hand.cards[0].value,
                                                        hand.cards[1].value,
                                                        hand.cards[2].value,
                                                        hand.cards[3].value,
                                                        hand.cards[4].value
                                                    );
                hand.result = GameUtils.findHandResult(hand.value, hand5Results);
                break;
            }
            case 6: {
                hand.cards = deck.getRandom6();

                const combinationResults = GameUtils.generateCombinationResults(card6combinations, hand.cards);

                hand.value = Math.min(...combinationResults);
                hand.result = GameUtils.findHandResult(hand.value, hand5Results);
                break;
            }
            case 7: {
                hand.cards = deck.getRandom7();

                const combinationResults = GameUtils.generateCombinationResults(card7combinations, hand.cards);

                hand.value = Math.min(...combinationResults);
                hand.result = GameUtils.findHandResult(hand.value, hand5Results);
                break;
            }
        }

        return hand;
    }
}

export default Game;
