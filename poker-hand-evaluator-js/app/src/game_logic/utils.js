import HandEvaluator from './HandEvaluator';

class GameUtils {
    static generateCombinationResults(combinations, allCards) {
        const handEvaluator = new HandEvaluator();
        const combinationResults = [];

        combinations.map(comboIndecies => {
            const hand5 = [];

            comboIndecies.map(comboIndex => {
                hand5.push(allCards[comboIndex]);
            });

            const hand5results = handEvaluator.eval5(
                                                        hand5[0].value,
                                                        hand5[1].value,
                                                        hand5[2].value,
                                                        hand5[3].value,
                                                        hand5[4].value
                                                    );

            combinationResults.push(hand5results);
        });

        return combinationResults;
    }

    static findHandResult(valueToFind, searchItems) {
        return searchItems.find(item => valueToFind >= item.minValue && valueToFind <= item.maxValue);
    }
}

export default GameUtils;
