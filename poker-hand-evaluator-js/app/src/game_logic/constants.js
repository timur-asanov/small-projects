const primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41];

const hand5Results = [
    { name: "Straight Flush", minValue: 0, maxValue: 10},
    { name: "Four of a Kind", minValue: 11, maxValue: 166},
    { name: "Full House", minValue: 167, maxValue: 322},
    { name: "Flush", minValue: 323, maxValue: 1599},
    { name: "Straight", minValue: 1600, maxValue: 1609},
    { name: "Three of a Kind", minValue: 1610, maxValue: 2467},
    { name: "Two Pair", minValue: 2468, maxValue: 3325},
    { name: "One Pair", minValue: 3326, maxValue: 6185},
    { name: "High Card", minValue: 6186, maxValue: 7462}
];

const hand3Results = [
    { name: "Three of a Kind", minValue: 0, maxValue: 12},
    { name: "One Pair", minValue: 13, maxValue: 168},
    { name: "High Card", minValue: 169, maxValue: 454}
];

const hand2Results = [
    { name: "One Pair", minValue: 0, maxValue: 12},
    { name: "High Card", minValue: 13, maxValue: 90}
];

const card6combinations = [[0,1,2,3,4], [0,1,2,3,5], [0,1,2,4,5], [0,1,3,4,5], [0,2,3,4,5], [1,2,3,4,5]];

const card7combinations = [
    [0,1,2,3,4],[0,1,2,3,5],[0,1,2,3,6],[0,1,2,4,5],[0,1,2,4,6],[0,1,2,5,6],
    [0,1,3,4,5],[0,1,3,4,6],[0,1,3,5,6],[0,1,4,5,6],[0,2,3,4,5],[0,2,3,4,6],
    [0,2,3,5,6],[0,2,4,5,6],[0,3,4,5,6],[1,2,3,4,5],[1,2,3,4,6],[1,2,3,5,6],
    [1,2,4,5,6],[1,3,4,5,6],[2,3,4,5,6]
];

export {
    primes,
    hand5Results,
    hand3Results,
    hand2Results,
    card6combinations,
    card7combinations
};
