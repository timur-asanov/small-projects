import Deck from './model/Deck';
import Hand from './model/Hand';
import { hand5Results, hand2Results, card6combinations, card7combinations } from './constants';
import HandEvaluator from './HandEvaluator';
import GameUtils from './utils';

class PokerGame {
    constructor() {
        this.handEvaluator = new HandEvaluator();

        this.deck = new Deck();
        this.hand = new Hand();
    }

    startGame() {
        this.hand.cards = this.deck.getRandom2();
        this.hand.value = this.handEvaluator.eval2(this.hand.cards[0].value, this.hand.cards[1].value);
        this.hand.result = GameUtils.findHandResult(this.hand.value, hand2Results);

        return this.hand;
    }

    flop() {
        const newCards = this.deck.getRandom3();

        this.hand.cards = this.hand.cards.concat(newCards);
        this.hand.value = this.handEvaluator.eval5(
                                                this.hand.cards[0].value,
                                                this.hand.cards[1].value,
                                                this.hand.cards[2].value,
                                                this.hand.cards[3].value,
                                                this.hand.cards[4].value
                                            );
        this.hand.result = GameUtils.findHandResult(this.hand.value, hand5Results);

        return this.hand;
    }

    turn() {
        const newCards = this.deck.getRandomCard();

        this.hand.cards = this.hand.cards.concat(newCards);

        const combinationResults = GameUtils.generateCombinationResults(card6combinations, this.hand.cards);

        this.hand.value = Math.min(...combinationResults);
        this.hand.result = GameUtils.findHandResult(this.hand.value, hand5Results);

        return this.hand;
    }

    river() {
        const newCards = this.deck.getRandomCard();

        this.hand.cards = this.hand.cards.concat(newCards);

        const combinationResults = GameUtils.generateCombinationResults(card7combinations, this.hand.cards);

        this.hand.value = Math.min(...combinationResults);
        this.hand.result = GameUtils.findHandResult(this.hand.value, hand5Results);

        return this.hand;
    }
}

export default PokerGame;
