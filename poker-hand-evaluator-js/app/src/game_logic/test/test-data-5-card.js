const highCards = [
    {
        value: 6969,
        cards: [
            { rank: 'KING', suit: 'HEARTS', value: 134228773 },
            { rank: 'NINE', suit: 'SPADES', value: 8394515 },
            { rank: 'FOUR', suit: 'CLUBS', value: 295429 },
            { rank: 'THREE', suit: 'HEARTS', value: 139523 },
            { rank: 'FIVE', suit: 'CLUBS', value: 557831 }
        ]
    },
    {
        value: 6301,
        cards: [
            { rank: 'KING', suit: 'HEARTS', value: 134228773 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'FIVE', suit: 'CLUBS', value: 557831 },
            { rank: 'NINE', suit: 'CLUBS', value: 8423187 },
            { rank: 'SEVEN', suit: 'SPADES', value: 2102541 }
        ]
    },
    {
        value: 7025,
        cards: [
            { rank: 'SIX', suit: 'DIAMONDS', value: 1065995 },
            { rank: 'QUEEN', suit: 'SPADES', value: 67115551 },
            { rank: 'JACK', suit: 'HEARTS', value: 33564957 },
            { rank: 'TEN', suit: 'CLUBS', value: 16812055 },
            { rank: 'FIVE', suit: 'DIAMONDS', value: 541447 }
        ]
    }
];

const onePairs = [
    {
        value: 5645,
        cards: [
            { rank: 'NINE', suit: 'SPADES', value: 8394515 },
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'FOUR', suit: 'CLUBS', value: 295429 },
            { rank: 'THREE', suit: 'DIAMONDS', value: 147715 },
            { rank: 'QUEEN', suit: 'DIAMONDS', value: 67127839 }
        ]
    },
    {
        value: 5248,
        cards: [
            { rank: 'FOUR', suit: 'SPADES', value: 266757 },
            { rank: 'JACK', suit: 'SPADES', value: 33560861 },
            { rank: 'SIX', suit: 'CLUBS', value: 1082379 },
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'SIX', suit: 'DIAMONDS', value: 1065995 }
        ]
    },
    {
        value: 3457,
        cards: [
            { rank: 'ACE', suit: 'HEARTS', value: 268446761 },
            { rank: 'JACK', suit: 'HEARTS', value: 33564957 },
            { rank: 'FIVE', suit: 'DIAMONDS', value: 541447 },
            { rank: 'ACE', suit: 'CLUBS', value: 268471337 },
            { rank: 'THREE', suit: 'CLUBS', value: 164099 }
        ]
    }
];

const twoPairs = [
    {
        value: 2712,
        cards: [
            { rank: 'KING', suit: 'CLUBS', value: 134253349 },
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'KING', suit: 'HEARTS', value: 134228773 },
            { rank: 'JACK', suit: 'DIAMONDS', value: 33573149 },
            { rank: 'TWO', suit: 'CLUBS', value: 98306 }
        ]
    },
    {
        value: 2683,
        cards: [
            { rank: 'KING', suit: 'SPADES', value: 134224677 },
            { rank: 'SEVEN', suit: 'CLUBS', value: 2131213 },
            { rank: 'KING', suit: 'HEARTS', value: 134228773 },
            { rank: 'FIVE', suit: 'HEARTS', value: 533255 },
            { rank: 'FIVE', suit: 'SPADES', value: 529159 }
        ]
    },
    {
        value: 2601,
        cards: [
            { rank: 'KING', suit: 'CLUBS', value: 134253349 },
            { rank: 'QUEEN', suit: 'DIAMONDS', value: 67127839 },
            { rank: 'KING', suit: 'SPADES', value: 134224677 },
            { rank: 'QUEEN', suit: 'CLUBS', value: 67144223 },
            { rank: 'JACK', suit: 'SPADES', value: 33560861 }
        ]
    }
];

const threeOfAKinds = [
    {
        value: 1645,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'TEN', suit: 'HEARTS', value: 16787479 },
            { rank: 'FOUR', suit: 'CLUBS', value: 295429 },
            { rank: 'ACE', suit: 'HEARTS', value: 268446761 }
        ]
    },
    {
        value: 2421,
        cards: [
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'KING', suit: 'SPADES', value: 134224677 },
            { rank: 'TWO', suit: 'SPADES', value: 69634 },
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'TWO', suit: 'HEARTS', value: 73730 }
        ]
    },
    {
        value: 1652,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'NINE', suit: 'HEARTS', value: 8398611 },
            { rank: 'FOUR', suit: 'CLUBS', value: 295429 },
            { rank: 'ACE', suit: 'HEARTS', value: 268446761 }
        ]
    }
];

const straights = [
    {
        value: 1608,
        cards: [
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'THREE', suit: 'SPADES', value: 135427 },
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'FIVE', suit: 'DIAMONDS', value: 541447 },
            { rank: 'SIX', suit: 'SPADES', value: 1053707 }
        ]
    },
    {
        value: 1609,
        cards: [
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'THREE', suit: 'CLUBS', value: 164099 },
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'FIVE', suit: 'HEARTS', value: 533255 }
        ]
    },
    {
        value: 1601,
        cards: [
            { rank: 'KING', suit: 'SPADES', value: 134224677 },
            { rank: 'TEN', suit: 'SPADES', value: 16783383 },
            { rank: 'JACK', suit: 'HEARTS', value: 33564957 },
            { rank: 'QUEEN', suit: 'DIAMONDS', value: 67127839 },
            { rank: 'NINE', suit: 'CLUBS', value: 8423187 }
        ]
    }
];

const flushes = [
    {
        value: 875,
        cards: [
            { rank: 'KING', suit: 'DIAMONDS', value: 134236965 },
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'FIVE', suit: 'DIAMONDS', value: 541447 },
            { rank: 'TEN', suit: 'DIAMONDS', value: 16795671 },
            { rank: 'QUEEN', suit: 'DIAMONDS', value: 67127839 }
        ]
    },
    {
        value: 1544,
        cards: [
            { rank: 'TEN', suit: 'HEARTS', value: 16787479 },
            { rank: 'FIVE', suit: 'HEARTS', value: 533255 },
            { rank: 'TWO', suit: 'HEARTS', value: 73730 },
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'SIX', suit: 'HEARTS', value: 1057803 }
        ]
    },
    {
        value: 877,
        cards: [
            { rank: 'KING', suit: 'DIAMONDS', value: 134236965 },
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'FOUR', suit: 'DIAMONDS', value: 279045 },
            { rank: 'TEN', suit: 'DIAMONDS', value: 16795671 },
            { rank: 'QUEEN', suit: 'DIAMONDS', value: 67127839 }
        ]
    }
];

const fullHouses = [
    {
        value: 176,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'FOUR', suit: 'CLUBS', value: 295429 },
            { rank: 'ACE', suit: 'HEARTS', value: 268446761 }
        ]
    },
    {
        value: 321,
        cards: [
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'FOUR', suit: 'SPADES', value: 266757 },
            { rank: 'TWO', suit: 'SPADES', value: 69634 },
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'TWO', suit: 'HEARTS', value: 73730 }
        ]
    },
    {
        value: 227,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'NINE', suit: 'SPADES', value: 8394515 },
            { rank: 'NINE', suit: 'HEARTS', value: 8398611 },
            { rank: 'NINE', suit: 'CLUBS', value: 8423187 },
            { rank: 'ACE', suit: 'HEARTS', value: 268446761 }
        ]
    }
];

const fourOfAKinds = [
    {
        value: 14,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'TEN', suit: 'HEARTS', value: 16787479 },
            { rank: 'ACE', suit: 'CLUBS', value: 268471337 },
            { rank: 'ACE', suit: 'HEARTS', value: 268446761 }
        ]
    },
    {
        value: 165,
        cards: [
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'TWO', suit: 'CLUBS', value: 98306 },
            { rank: 'TWO', suit: 'SPADES', value: 69634 },
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'TWO', suit: 'HEARTS', value: 73730 }
        ]
    },
    {
        value: 13,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'JACK', suit: 'HEARTS', value: 33564957 },
            { rank: 'ACE', suit: 'CLUBS', value: 268471337 },
            { rank: 'ACE', suit: 'HEARTS', value: 268446761 }
        ]
    }
];

const straightFlushes = [
    {
        value: 9,
        cards: [
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'FOUR', suit: 'DIAMONDS', value: 279045 },
            { rank: 'SIX', suit: 'DIAMONDS', value: 1065995 },
            { rank: 'THREE', suit: 'DIAMONDS', value: 147715 },
            { rank: 'FIVE', suit: 'DIAMONDS', value: 541447 }
        ]
    },
    {
        value: 10,
        cards: [
            { rank: 'TWO', suit: 'SPADES', value: 69634 },
            { rank: 'THREE', suit: 'SPADES', value: 135427 },
            { rank: 'FOUR', suit: 'SPADES', value: 266757 },
            { rank: 'FIVE', suit: 'SPADES', value: 529159 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 }
        ]
    },
    {
        value: 2,
        cards: [
            { rank: 'KING', suit: 'CLUBS', value: 134253349 },
            { rank: 'TEN', suit: 'CLUBS', value: 16812055 },
            { rank: 'JACK', suit: 'CLUBS', value: 33589533 },
            { rank: 'QUEEN', suit: 'CLUBS', value: 67144223 },
            { rank: 'NINE', suit: 'CLUBS', value: 8423187 }
        ]
    }
];

export {
    highCards,
    onePairs,
    twoPairs,
    threeOfAKinds,
    straights,
    flushes,
    fullHouses,
    fourOfAKinds,
    straightFlushes
};
