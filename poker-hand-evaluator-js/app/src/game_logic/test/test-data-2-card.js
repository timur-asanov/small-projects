const onePairs_2card = [
    {
        value: 0,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 }
        ]
    },
    {
        value: 6,
        cards: [
            { rank: 'EIGHT', suit: 'DIAMONDS', value: 4212241 },
            { rank: 'EIGHT', suit: 'HEARTS', value: 4204049 }
        ]
    },
    {
        value: 12,
        cards: [
            { rank: 'TWO', suit: 'HEARTS', value: 73730 },
            { rank: 'TWO', suit: 'CLUBS', value: 98306 }
        ]
    }
];

const highCards_2card = [
    {
        value: 13,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'KING', suit: 'CLUBS', value: 134253349 }
        ]
    },
    {
        value: 48,
        cards: [
            { rank: 'QUEEN', suit: 'DIAMONDS', value: 67127839 },
            { rank: 'FIVE', suit: 'HEARTS', value: 533255 }
        ]
    },
    {
        value: 90,
        cards: [
            { rank: 'TWO', suit: 'CLUBS', value: 98306 },
            { rank: 'THREE', suit: 'DIAMONDS', value: 147715 }
        ]
    }
];

export {
    onePairs_2card,
    highCards_2card
};
