/* left here for reference on how the lookup arrays for 2 card and 3 card hands were generated */

// /* eslint-disable no-console */

// import Hand from '../model/Hand';
// import { generatedCards } from '../generated';

// class Utils {
//     static generate3HandResults() {
//         const cards = "TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE,TEN,JACK,QUEEN,KING,ACE".split(",");
//         const handSize = 3;

//         const combinations = compute(cards, handSize);

//         const highCardHands = [];
//         const kind3Hands = [];
//         const onePairHands = [];

//         combinations.map(result => {
//             const card1 = generatedCards.find(item => result[0] === item.rank);
//             const card2 = generatedCards.find(item => result[1] === item.rank);
//             const card3 = generatedCards.find(item => result[2] === item.rank);

//             const hand = new Hand([card1, card2, card3], (card1.value & 0xff) * (card2.value & 0xff) * (card3.value & 0xff));

//             /*
//              * need to separate 3 of a kinds, one pairs and high card hands
//              */
//             if (result[0] === result[1] && result[1] === result[2]) {
//                 kind3Hands.push(hand);
//             } else if (result[0] === result[1] || result[0] === result[2] || result[1] === result[2]) {
//                 onePairHands.push(hand);
//             } else {
//                 highCardHands.push(hand);
//             }
//         });

//         /*
//          * There are 286 hands of 3 cards that are high card. The values range from 30 to 47027
//          */
//         const sortedHighCardHands = highCardHands.sort((a, b) => a.value - b.value).slice(0).reverse();
//         const highCardResultsArray = [];

//         sortedHighCardHands.map(sortedHand => {
//             highCardResultsArray.push(sortedHand.value);
//         });

//         /*
//          * There are 13 hands of 3 cards that are three of a kind. The values are:
//          * - 68921, 50653, 29791, 24389, 12167, 6859, 4913, 2197, 1331, 343, 125, 27, 8
//          */
//         const sortedKind3Hands = kind3Hands.sort((a, b) => a.value - b.value).slice(0).reverse();
//         const kind3ResultsArray = [];

//         sortedKind3Hands.map(sortedKind3Hand => {
//             kind3ResultsArray.push(sortedKind3Hand.value);
//         });

//         /*
//          * There are 156 hands of 3 cards that are one pair.
//          */
//         const sortedOnePairHands = onePairHands.sort((a, b) => a.value - b.value).slice(0).reverse();
//         const onePairResultsArray = [];

//         sortedOnePairHands.map(sortedOnePairHand => {
//             onePairResultsArray.push(sortedOnePairHand.value);
//         });

//         /*
//          * To get the look up array, combine the sorted array above with:
//          *
//          * - index 0 to index 12: three of a kind values
//          * - index 13 to index 168: one pairs values
//          * - index 169 to index 454: high card values
//          */
//         const finalResult = kind3ResultsArray.concat(onePairResultsArray, highCardResultsArray);

//         console.log("[" + finalResult.join(", ") + "]");
//     }

//     static generate2HandResults() {
//         const cards = "TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE,TEN,JACK,QUEEN,KING,ACE".split(",");
//         const handSize = 2;

//         const combinations = compute(cards, handSize);

//         const highCardHands = [];
//         const onePairHands = [];

//         combinations.map(result => {
//             const card1 = generatedCards.find(item => result[0] === item.rank);
//             const card2 = generatedCards.find(item => result[1] === item.rank);

//             const hand = new Hand([card1, card2], (card1.value & 0xff) * (card2.value & 0xff));

//             /*
//              * need to separate one pairs and high card hands
//              */
//             if (result[0] === result[1]) {
//                 onePairHands.push(hand);
//             } else {
//                 highCardHands.push(hand);
//             }
//         });

//         /*
//          * There are 78 hands of 3 cards that are high card. The values range from 6 to 1517
//          */
//         const sortedHighCardHands = highCardHands.sort((a, b) => a.value - b.value).slice(0).reverse();
//         const highCardResultsArray = [];

//         sortedHighCardHands.map(sortedHand => {
//             highCardResultsArray.push(sortedHand.value);
//         });

//         /*
//          * There are 13 hands of 2 cards that are one pairs. The values are:
//          * - 1681, 1369, 961, 841, 529, 361, 289, 169, 121, 49, 25, 9, 4,
//          */
//         const sortedOnePairHands = onePairHands.sort((a, b) => a.value - b.value).slice(0).reverse();
//         const onePairResultsArray = [];

//         sortedOnePairHands.map(sortedOnePairHand => {
//             onePairResultsArray.push(sortedOnePairHand.value);
//         });

//         /*
//          * To get the look up array, combine the sorted array above with:
//          *
//          * - index 0 to index 12: one pairs values
//          * - index 13 to index 90: high card values
//          */
//         const finalResult = onePairResultsArray.concat(highCardResultsArray);

//         console.log("[" + finalResult.join(", ") + "]");
//     }
// }

// export default Utils;

// /*
//  * Modified version of https://gitlab.com/snippets/1747591
//  */
// function compute(itemsPool, itemsNeeded) {
//     const results = [];
//     const stack = [];
//     stack.push([itemsNeeded, itemsPool]);

//     const temp4 = [];

//     while (stack.length > 0) {
//         const topItem = stack.pop();
//         const itemsNeededNext = topItem[0];
//         const topArray = topItem[1];

//         temp4[itemsNeededNext] = topArray[0];

//         if (itemsNeededNext === 1) {
//             const temp1 = [];

//             for (let i = itemsNeeded; i > 0; i--) {
//                 temp1.push(temp4[i]);
//             }

//             results.push(temp1);
//         }

//         const temp2 = topArray.slice(0);
//         temp2.splice(0, 1);

//         if (temp2.length > 0) {
//             if (itemsNeededNext > 0) {
//                 stack.push([itemsNeededNext, temp2]);
//             }
//         }

//         if (topArray.length > 0) {
//             if (itemsNeededNext > 1) {
//                 stack.push([itemsNeededNext - 1, topArray]);
//             }
//         }
//     }

//     return results;
// }
