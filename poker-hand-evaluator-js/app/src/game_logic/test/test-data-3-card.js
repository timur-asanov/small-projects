const threeOfAKinds_3card = [
    {
        value: 0,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'ACE', suit: 'CLUBS', value: 268471337 }
        ]
    },
    {
        value: 12,
        cards: [
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 },
            { rank: 'TWO', suit: 'SPADES', value: 69634 },
            { rank: 'TWO', suit: 'HEARTS', value: 73730 }
        ]
    },
    {
        value: 12,
        cards: [
            { rank: 'TWO', suit: 'HEARTS', value: 73730 },
            { rank: 'TWO', suit: 'CLUBS', value: 98306 },
            { rank: 'TWO', suit: 'DIAMONDS', value: 81922 }
        ]
    }
];

const onePairs_3card = [
    {
        value: 13,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'KING', suit: 'CLUBS', value: 134253349 }
        ]
    },
    {
        value: 48,
        cards: [
            { rank: 'EIGHT', suit: 'DIAMONDS', value: 4212241 },
            { rank: 'ACE', suit: 'SPADES', value: 268442665 },
            { rank: 'EIGHT', suit: 'HEARTS', value: 4204049 }
        ]
    },
    {
        value: 168,
        cards: [
            { rank: 'TWO', suit: 'HEARTS', value: 73730 },
            { rank: 'TWO', suit: 'CLUBS', value: 98306 },
            { rank: 'THREE', suit: 'DIAMONDS', value: 147715 }
        ]
    }
];

const highCards_3card = [
    {
        value: 169,
        cards: [
            { rank: 'ACE', suit: 'DIAMONDS', value: 268454953 },
            { rank: 'QUEEN', suit: 'SPADES', value: 67115551 },
            { rank: 'KING', suit: 'CLUBS', value: 134253349 }
        ]
    },
    {
        value: 309,
        cards: [
            { rank: 'QUEEN', suit: 'DIAMONDS', value: 67127839 },
            { rank: 'SIX', suit: 'SPADES', value: 1053707 },
            { rank: 'FIVE', suit: 'HEARTS', value: 533255 }
        ]
    },
    {
        value: 454,
        cards: [
            { rank: 'FOUR', suit: 'HEARTS', value: 270853 },
            { rank: 'TWO', suit: 'CLUBS', value: 98306 },
            { rank: 'THREE', suit: 'DIAMONDS', value: 147715 }
        ]
    }
];

export {
    threeOfAKinds_3card,
    onePairs_3card,
    highCards_3card
};
