import { hand5Results, hand3Results, hand2Results } from './constants';
import GameUtils from './utils';

test('evaluated valid 5 hand results', () => {
    const testData = [
        { result: hand5Results[0].name, values: [0, 1, 3, 8, 10] },
        { result: hand5Results[1].name, values: [11, 99, 132, 166] },
        { result: hand5Results[2].name, values: [167, 168, 289, 322]},
        { result: hand5Results[3].name, values: [323, 758, 890, 1599] },
        { result: hand5Results[4].name, values: [1600, 1601, 1605, 1609] },
        { result: hand5Results[5].name, values: [1610, 1800, 2333, 2467] },
        { result: hand5Results[6].name, values: [2468, 2964, 3254, 3325] },
        { result: hand5Results[7].name, values: [3326, 3567, 5555, 6185] },
        { result: hand5Results[8].name, values: [6186, 6222, 7158, 7462] }
    ];

    testData.map((scenario) => {
        scenario.values.map((value) => {
            const result = GameUtils.findHandResult(value, hand5Results);

            expect(result).toBeDefined();
            expect(result.name).toBe(scenario.result);
        });
    });

    expect(GameUtils.findHandResult('500', hand5Results)).toBeDefined();
    expect(GameUtils.findHandResult('500   ', hand5Results)).toBeDefined();
    expect(GameUtils.findHandResult('    500', hand5Results)).toBeDefined();
    expect(GameUtils.findHandResult('   500   ', hand5Results)).toBeDefined();
    expect(GameUtils.findHandResult('  500   ', hand5Results).name).toBe(hand5Results[3].name);
    expect(GameUtils.findHandResult(500.5, hand5Results)).toBeDefined();
});

test('evaluated invalid 5 hand results', () => {
    const testData = [
        {result: hand5Results[0].name, values: [11, 150, 1000]},
        {result: hand5Results[1].name, values: [10, 167, 1112]},
        {result: hand5Results[2].name, values: [166, 323, 500]},
        {result: hand5Results[3].name, values: [322, 1600, 300]},
        {result: hand5Results[4].name, values: [1599, 1610, 1611]},
        {result: hand5Results[5].name, values: [1609, 2468, 1600]},
        {result: hand5Results[6].name, values: [2467, 3326, 89]},
        {result: hand5Results[7].name, values: [3325, 6186, 11]},
        {result: hand5Results[8].name, values: [6185, 500, 1500]}
    ];

    testData.map((scenario) => {
        scenario.values.map((value) => {
            expect(GameUtils.findHandResult(value, hand5Results).name).not.toBe(scenario.result);
        });
    });

    expect(GameUtils.findHandResult(7463, hand5Results)).toBeUndefined();
    expect(GameUtils.findHandResult(10000, hand5Results)).toBeUndefined();
    expect(GameUtils.findHandResult(-1, hand5Results)).toBeUndefined();
    expect(GameUtils.findHandResult('a', hand5Results)).toBeUndefined();
    expect(GameUtils.findHandResult('test', hand5Results)).toBeUndefined();
    expect(GameUtils.findHandResult('1-500', hand5Results)).toBeUndefined();
    expect(GameUtils.findHandResult('2 500', hand5Results)).toBeUndefined();
});

test('evaluated valid 3 hand results', () => {
    const testData = [
        { result: hand3Results[0].name, values: [0, 2, 3, 8, 12] },
        { result: hand3Results[1].name, values: [13, 14, 150, 168] },
        { result: hand3Results[2].name, values: [169, 199, 392, 454]}
    ];

    testData.map((scenario) => {
        scenario.values.map((value) => {
            const result = GameUtils.findHandResult(value, hand3Results);

            expect(result).toBeDefined();
            expect(result.name).toBe(scenario.result);
        });
    });

    expect(GameUtils.findHandResult('400', hand3Results)).toBeDefined();
    expect(GameUtils.findHandResult('400   ', hand3Results)).toBeDefined();
    expect(GameUtils.findHandResult('    400', hand3Results)).toBeDefined();
    expect(GameUtils.findHandResult('   400   ', hand3Results)).toBeDefined();
    expect(GameUtils.findHandResult('  155   ', hand3Results).name).toBe(hand3Results[1].name);
    expect(GameUtils.findHandResult(400.5, hand3Results)).toBeDefined();
});

test('evaluated invalid 3 hand results', () => {
    const testData = [
        {result: hand3Results[0].name, values: [13, 150, 300]},
        {result: hand3Results[1].name, values: [5, 169, 186]},
        {result: hand3Results[2].name, values: [7, 93, 130]}
    ];

    testData.map((scenario) => {
        scenario.values.map((value) => {
            expect(GameUtils.findHandResult(value, hand3Results).name).not.toBe(scenario.result);
        });
    });

    expect(GameUtils.findHandResult(455, hand3Results)).toBeUndefined();
    expect(GameUtils.findHandResult(10000, hand3Results)).toBeUndefined();
    expect(GameUtils.findHandResult(-1, hand3Results)).toBeUndefined();
    expect(GameUtils.findHandResult('a', hand3Results)).toBeUndefined();
    expect(GameUtils.findHandResult('test', hand3Results)).toBeUndefined();
    expect(GameUtils.findHandResult('1-400', hand3Results)).toBeUndefined();
    expect(GameUtils.findHandResult('2 500', hand3Results)).toBeUndefined();
});

test('evaluated valid 2 hand results', () => {
    const testData = [
        { result: hand2Results[0].name, values: [0, 2, 3, 8, 12] },
        { result: hand2Results[1].name, values: [13, 14, 77, 90] }
    ];

    testData.map((scenario) => {
        scenario.values.map((value) => {
            const result = GameUtils.findHandResult(value, hand2Results);

            expect(result).toBeDefined();
            expect(result.name).toBe(scenario.result);
        });
    });

    expect(GameUtils.findHandResult('50', hand2Results)).toBeDefined();
    expect(GameUtils.findHandResult('50   ', hand2Results)).toBeDefined();
    expect(GameUtils.findHandResult('    50', hand2Results)).toBeDefined();
    expect(GameUtils.findHandResult('   50   ', hand2Results)).toBeDefined();
    expect(GameUtils.findHandResult('  88   ', hand2Results).name).toBe(hand2Results[1].name);
    expect(GameUtils.findHandResult(50.5, hand2Results)).toBeDefined();
});

test('evaluated invalid 2 hand results', () => {
    const testData = [
        {result: hand2Results[0].name, values: [13, 63, 90]},
        {result: hand2Results[1].name, values: [1, 5, 10]}
    ];

    testData.map((scenario) => {
        scenario.values.map((value) => {
            expect(GameUtils.findHandResult(value, hand2Results).name).not.toBe(scenario.result);
        });
    });

    expect(GameUtils.findHandResult(455, hand2Results)).toBeUndefined();
    expect(GameUtils.findHandResult(10000, hand2Results)).toBeUndefined();
    expect(GameUtils.findHandResult(-1, hand2Results)).toBeUndefined();
    expect(GameUtils.findHandResult('a', hand2Results)).toBeUndefined();
    expect(GameUtils.findHandResult('test', hand2Results)).toBeUndefined();
    expect(GameUtils.findHandResult('1-400', hand2Results)).toBeUndefined();
    expect(GameUtils.findHandResult('2 500', hand2Results)).toBeUndefined();
});
